const exec = require('child_process').exec;
const gulp = require('gulp'); 
const fs = require("fs");
const rimraf = require("rimraf");

function pjson(){
    let rawdata = fs.readFileSync('./package.json');
    return JSON.parse(rawdata);
    }

gulp.task('make', function (cb) {
    const action = ()=>{
        exec("tsc",function(err,stdout,stderr){
            console.log(stdout);
            console.error(stderr);
            cb();
        });
    }
    
    if(!fs.existsSync("./build")){
        fs.mkdirSync("./build");
        console.log("Created ./build");
        action();
    }
    else{
        rimraf("./build", function () { 
            console.log("Cleared ./build");
            fs.mkdirSync("./build");
            action();
         });
        
    }
    

   
})
//pkg build/index.js

gulp.task('build', function (cb) {
    const name = pjson().display_name;
    const version = pjson().version;
    const action = ()=>{

        exec("pkg build/index.js --targets=node14-win-x64 -o dist/"+name+"_"+version+".exe",function(err,stdout,stderr){
            console.log(stdout);
            console.error(stderr);
            exec("pkg build/index.js --targets=node14-linux-x64 -o dist/"+name+"_"+version+"_x64",function(err,stdout,stderr){
                console.log(stdout);
                console.error(stderr);
                exec("pkg build/index.js --targets=node14-linux-arm64 -o dist/"+name+"_"+version+"_arm64",function(err,stdout,stderr){
                    console.log(stdout);
                    console.error(stderr);
                    cb();
                });
            });
        });
    }
    
    if(!fs.existsSync("./dist")){
        fs.mkdirSync("./dist");
        console.log("Created ./dist");
        action();
    }
    else{
        rimraf("./dist", function () { 
            console.log("Cleared ./dist");
            fs.mkdirSync("./dist");
            action();
         });
        
    }
    

   
})