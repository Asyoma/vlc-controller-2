import { exec } from 'child_process';
import { CONSOLE_LOG } from "../console.log";
import { EVENT_EMITTER } from "../eventEmitter";
export class Command{
    constructor(){
        EVENT_EMITTER.on("CMD",data=>{
            exec(data, (err, stdout, stderr) => {
                if (err) {
                  // node couldn't execute the command
                  CONSOLE_LOG.warn("couldn't execute the command");
                  return;
                }
              
                CONSOLE_LOG.info(`stdout: ${stdout}`);
                CONSOLE_LOG.error(`stderr: ${stderr}`);
              });
        })
    }
}