import { EVENT_EMITTER } from "./eventEmitter";
import { CONSOLE_LOG } from "./console.log";
import { Board, Sensor } from "johnny-five";
class Arduino{
    input_pins_arr=[];
    board;
    constructor(cb,settings,pins_arr = []){

        if(pins_arr.length > 0){
            CONSOLE_LOG.warn("Arduino settings = "+JSON.stringify(settings));
            (async ()=>{
                this.board = await new Board({
                    port: settings.port,
                    freq:settings.freq,
                    threshold:settings.threshold
                });
                this.board.on("ready", () => {
                    pins_arr.forEach(pin=>{
                        this.input_pins_arr.push({pin:pin,sensor:new Sensor(pin)});
                        if(pins_arr.indexOf(pin) == pins_arr.length-1){
                            this.input_pins_arr.forEach(input_pin=>{
                                input_pin.sensor.on("change",value => {
                                    EVENT_EMITTER.emit(input_pin.pin,value);
                                })
                                if(this.input_pins_arr.indexOf(input_pin) == this.input_pins_arr.length-1){
                                    cb(true);
                                }
                            });
                        }
                    });
                });
                
            })();
            
        }else{
            CONSOLE_LOG.warn("No Arduino PINS! Shutdown!");
           cb(false);
        }
    }
}

export{
    Arduino,
    ArduinoValues
}

enum ArduinoValues {
    Digital_Low = 0,
    Digital_High = 1,
    Analog_Min = 0,
    Analog_Max = 1023,
  }