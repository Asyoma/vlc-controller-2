import { CONSOLE_LOG } from "./console.log";
import { ArduinoValues } from './arduino';
import * as data_store from 'data-store';
import * as os from "os";
import * as path from "path";

const _settings ={
    port:"/dev/ttyUSB0",
    freq:25,
    threshold:1,
    needRunVlc:true,
    
    vlc_options:{
        exts:["mp4","avi","MP4","AVI","m4v"],
        media_files_folder_location:path.join(os.homedir(),"Videos").split(path.sep).join("/"),
        fullscreen:true,
        volume:100,
        random:false,
    },
    vlc:{
        "vlc_cmd":"vlc",
        params:{
            no_osd:true,
            no_mouse_events:false,
            no_video_deco:false,
            no_embedded_video:false
        },
        "additional_params":[],
        "host_for_exec":"localhost",
        "host_for_controller":"localhost",
        port:8888,
        password:"1234"
    }
};
const _pins = {
    onplay:{
        status:false,
        command:{
            type:"VLC",
            value:"play"
        },
        input:{
            type:"on_start",
        },
        options:{
            loop:true,
            startAfter:0,
            lockdown:{
                status:true,
                time:5
            }
        },
        
    },
    
    analog:{
        status:false,
        command:{
            type:"CMD",
            value:"notepad"
        },
        input:{
            type:"analog_single",
            pin:"A0"
        },
        value:{
            action:{
                min:501,
                max:1023
            },
            non_action:{
                min:0,
                max:500
            },
            lockdown:{
                status:true,
                time:5
            },
            readOnly:true,
            showAll:false
            
        }
    },
    dual_input:{
        status:false,
        command:{
            type:"VLC",
            value:"play"
        },
        input:{
            type:"analog_dual",
            pins:{
                1:"A1",
                2:"A2"
            }
        },
        value:{
            action:{
                min:501,
                max:1023
            },
            non_action:{
                min:0,
                max:500
            },
            lockdown:{
                status:true,
                time:5
            },
            readOnly:true,
            showAll:false
            
        }
    },
    
};
export class Settings{
    store=null;
    constructor(main_cb){
        this.store = data_store({ path: process.cwd() + '/settings.json' });
        
        (async ()=>{

            await new Promise<void>((resolve, reject) => {
                const arr = Object.keys(_pins);
                if(!this.store.has("pins")){
                    arr.forEach(setting_element => {
                        CONSOLE_LOG.info(setting_element);
                        if(!this.store.has("pins."+setting_element)){
                            this.store.set("pins."+setting_element,_pins[setting_element]);
                        }
                        if(arr.indexOf(setting_element) == arr.length-1){
                            resolve();
                        }
                    });
                }
                else{
                    resolve();
                }
            });

            await new Promise<void>((resolve, reject) => {
                const arr = Object.keys(_settings);
                arr.forEach(setting_element => {
                    
                    if(!this.store.has("settings."+setting_element)){
                        this.store.set("settings."+setting_element,_settings[setting_element]);
                    }
                    if(arr.indexOf(setting_element) == arr.length-1){
                        resolve();
                    }
                });
            });

            main_cb();


        })();
        
    }
    getSetting(key){
        if(this.store != null){
            
            if(this.store.has("settings."+key)){
                CONSOLE_LOG.info("Setting exist = "+key)
                return this.store.get("settings."+key)
            }
            else{
                CONSOLE_LOG.info("Setting not exist = "+key)
                this.store.set("settings."+key,_settings[key]);
                try{
                    return _settings[key];
                }
                catch(e){
                    return null;
                }
            }
        }
        else{
            this.store.set("settings."+key,_settings[key]);
            try{
                return _settings[key];
            }
            catch(e){
                return null;
            }
            
        }
    }
    getAllSetting(cb){
        if(this.store != null){
            
            if(this.store.has("settings")){
                return this.store.get("settings")
            }
            else{
                return null;
            }
        }
        else{
            
            try{
                return _settings;
            }
            catch(e){
                return null;
            }
        }
    }
    getPin(key){
        if(this.store != null){
            
            if(!this.store.has("pins."+key)){
                return this.store.get("pins."+key)
            }
            else{
                this.store.set("pins."+key,_pins[key]);
                try{
                    return _pins[key];
                }
                catch(e){
                    return null;
                }
            }
        }
        else{
            this.store.set("pins."+key,_pins[key]);
            try{
                return _pins[key];
            }
            catch(e){
                return null;
            }
        }
    }
    getAllPins(){
        if(this.store != null){
            
            if(this.store.has("pins")){
                return this.store.get("pins");
            }
            else{
                return null;
            }
        }
        else{
            try{
                return _pins;
            }
            catch(e){
                return null;
            }
        }
    }
}