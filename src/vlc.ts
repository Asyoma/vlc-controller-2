import { EVENT_EMITTER } from "./eventEmitter";
import { CONSOLE_LOG } from "./console.log";
import * as path from "path";
import * as cron from 'node-cron';
import * as VLC from "vlc-client"
import * as glob from 'fast-glob';
import { spawn } from 'child_process';

class Vlc{
    isWorking = false;
    isConnected=false;
    isPlaying = false;
    isDetectingMode =false;
    isFirst = true;
    vlc_options=[];
    vlc_settings;
    vlc;
    media_files=[];

    constructor(vlc_settings,vlc_options=null,needRunVlc=false){
        this.vlc_settings = vlc_settings;
        this.vlc_options=vlc_options;
        if(needRunVlc == true){
            new VlcExec(vlc_settings,vlc_options);
            setTimeout(()=>{
                this.init();
            },500);
        }
        else{
            this.init();
        }
    }

    private async init(){
        this.vlc = new VLC.Client({
            ip:this.vlc_settings.host_for_controller,
            port:this.vlc_settings.port,
            username: "",
            password:this.vlc_settings.password
        });
        await this.setVolume(0);
        await this.vlc.setLooping(true);
        this.media_files = await this.getVideoFiles();
        
        

        cron.schedule('* * * * * *', () => {
            if(this.media_files.length>0 && this.isWorking == false){
                CONSOLE_LOG.info("VLC media Files = "+JSON.stringify(this.media_files));
                if(this.isConnected == false){
                    (async ()=>{
                        try{
                            const s = await this.vlc.status();
                            this.isConnected =true;
                            //console.log(s);
                            this.afterConnection();
                        }
                        catch(e){}
                        
                    })();
                    
                }else{
                    (async ()=>{
                        const progress = await this.vlc.getProgress();
                        //console.log(progress);
                        
                        if(this.isDetectingMode == true){
                            if(progress >= 99.5){
                                await this.setVolume(0);
                            }
                            if(progress <=2){
                                this.isPlaying = false;
                                this.isDetectingMode = false;
                                
                                if(this.isFirst == true){
                                    this.isFirst = false;
                                }
                                else{
                                    EVENT_EMITTER.emit("VLC","ended");
                                }
                            }
                        }
                        else{
                            if(progress >=5){
                                this.isDetectingMode = true;
                            }
                        }
                        const currentVlcIsPlaying = await this.vlc.isPlaying();
                        if(this.isPlaying == false && currentVlcIsPlaying == true){
                            await this.vlc.pause();
                            await this.setVolume(0);
                            await this.vlc.setTime(0);
                            EVENT_EMITTER.emit("VLC","ended");
                        }
                        else if(this.isPlaying == true && currentVlcIsPlaying == false){
                            await this.setVolume(Number(this.vlc_options["volume"]));
                            await this.vlc.play();
                            
                        }
                        if(this.isPlaying == false){
                            await this.setVolume(0);
                        }
    
                    })();
                }
            }
            else{
                if(this.isWorking == false){
                    this.isWorking = true;
                    (async ()=>{
                        this.media_files = await this.getVideoFiles();
                        setTimeout(()=>{
                            this.isWorking = false;
                        },1000);
                    })();
                }
                
                
            }
            
        });

    }

    private async setVolume(vol:number=0){
        if(vol == 0){
            await this.vlc.setVolume(0);
        }
        else{
            const x = Math.round(map(vol,0,100,0,50));
            await this.vlc.setVolume(x);
        }
    }
    private afterConnection(){

        if(this.vlc_options["fullscreen"] == false){
            this.vlc.setFullscreen(false);
        }
        else{
            this.vlc.setFullscreen(true);
        }
        
        this.vlc.emptyPlaylist().then(s=>{
            this.media_files.forEach(file=>{
                this.vlc.playFile(file)
                if(this.media_files.indexOf(file) == this.media_files.length-1){
                    
                    
                    this.vlc.getPlaylist().then(list=>{
                        
                        this.vlc.playFromPlaylist(list[0]["id"]);
                        setTimeout(()=>{
                            EVENT_EMITTER.emit("VLC","ready");
                            this.initEventListener();
                        },1000);
                        
                    })
                }
                
            });
        }).catch(e=>{});
        
    }

    private initEventListener(){
        EVENT_EMITTER.on("VLC",data=>{
            switch(data){
                case "play":
                    if(this.isPlaying ==false){
                        this.isPlaying = true;
                        this.setVolume(Number(this.vlc_options["volume"]));
                    }
                    
                break;
            }
        })
    }

    

    private getVideoFiles(){
        let promises = [];
        let t_playlist =[];
        
        this.vlc_options["exts"].forEach((ext)=>{
            promises.push(new Promise<void>((resolve, reject) => {
                    glob(this.vlc_options["media_files_folder_location"]+"/*."+ext, { dot: true }).then(files=>{
                        if(files.length>0){
                            files.forEach(file=>{
                                const f = file.split("/").join(path.sep);
                                t_playlist.push(f);
                                if(files.indexOf(file) == files.length-1){
                                    resolve();
                                }
                            });
                        }
                        else{
                            resolve();
                        }
                    }).catch(e=>{
                        resolve();
                    });
            }));
            
           
        });
        return Promise.all(promises).then(function() {
            return t_playlist;
        });
        
    }
}

class VlcExec{
    cp_vlc;
    constructor(vlc_settings,vlc_options){
        let params = [
            "-I","qt",
            "--extraintf", "http",
            "--http-port", vlc_settings.port,
            "--http-host", vlc_settings.host_for_exec,
            "--http-password",vlc_settings.password
        ];
        if(vlc_options.fullscreen == true){
            params.push("--fullscreen");
        }
        if(vlc_settings.params.no_osd == true){
            params.push("--no-osd");
        }
        if(vlc_settings.params.no_video_deco == true){
            params.push("--no-video-deco");
        }
        if(vlc_settings.params.no_mouse_events == true){
            params.push("--no-mouse-events");
        }
        if(vlc_settings.params.no_embedded_video == true){
            params.push("--no-embedded-video");
        }
        
        if(vlc_settings.additional_params.length>0){
            params = params.concat(vlc_settings.additional_params)
        }
        CONSOLE_LOG.info("VLC params = "+JSON.stringify(params));

        this.cp_vlc = spawn(vlc_settings.vlc_cmd,params);

        this.cp_vlc.stdout.on('data', (data) => {
            CONSOLE_LOG.info(`child stdout:\n${data}`);
        });

        this.cp_vlc.stderr.on('data', function(data) {
            CONSOLE_LOG.info(data.toString());
        });
        this.cp_vlc.on('exit', function(code){
            CONSOLE_LOG.info('Exit code: ' + code);
        });
    }
    
}

export{
    Vlc,
    VlcExec
}

function map(x, in_min, in_max, out_min, out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}