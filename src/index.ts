import { Main } from  "./main.js";

class Index{
    isDevelop = false;
    isVLCExec = false;
    constructor(){
        process.argv.forEach( (val, index, array)=> {
            try{
                switch(val){
                  case "--develop":
                    this.isDevelop = true;
                  break;
                  case "--execVLC":
                    this.isVLCExec = true;
                  break;
                }
              }
              catch(err){
                console.error(err);
              }
              if(index == process.argv.length-1){
                new Main(this.isDevelop,this.isVLCExec);
                
                
                  
              }
        })
    }
}

new Index();
