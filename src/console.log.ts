import * as ololog from 'ololog';

const CONSOLE_LOG = ololog.configure ({ 
    locate: true,
    time: true
    
 })

 export{
    CONSOLE_LOG
}