
import { CONSOLE_LOG } from "./console.log";
import { EVENT_EMITTER } from "./eventEmitter";
import { Command } from "./Actions/command";
import { Arduino } from "./arduino";
import { Settings } from "./settings";
import { Readers } from "./readers";
import { Vlc,VlcExec } from "./vlc";


export class Main{
    _pins
    settings = null;
    isVLCExec=false;
    constructor(isDevelop=false,isVLCExec=false){
        this.isVLCExec=isVLCExec;
        CONSOLE_LOG.info("App Started!");
        (async ()=>{
            this.settings = await new Settings(async ()=>{
                if(this.isVLCExec == false){
                    this._pins = await this.settings.getAllPins();
                    this.init();
                }
                else{
                    new VlcExec(await this.settings.getSetting("vlc"),await this.settings.getSetting("vlc_options"));
                }
                
            });
        })();
        
        
    }
    private async init(){
            
            
            new Vlc(await this.settings.getSetting("vlc"),await this.settings.getSetting("vlc_options"),this.isVLCExec == false?await this.settings.getSetting("needRunVlc"):false);
            EVENT_EMITTER.on("VLC",async d=>{
                switch(d){
                    case "ready":
                        const filtered_pins = await this.getAllPinsFiltered();
                        CONSOLE_LOG.info("ARDUINO pins = "+filtered_pins);
                        new Readers(this._pins);
                        new Command();
                        if(filtered_pins.length>0){
                                new Arduino(async arduino_stat=>{
                                    if(arduino_stat == false){
                                        process.exit(0);
                                    }
                                },{
                                    port: await this.settings.getSetting("port"),
                                    freq: await this.settings.getSetting("freq"),
                                    threshold: await this.settings.getSetting("threshold"),
                                },filtered_pins);
                            
                            
                        }
                    break;
                }
            });
            
        
    }
    private async getAllPinsFiltered(){
        let promises = [];
        let list = [];
        
        const pins = await Object.keys(this._pins);

        if(pins.length>0){
            pins.forEach(pin=>{
                promises.push(new Promise<void>((resolve, reject) => {
                    if(this._pins[pin].status == true){
                        switch(this._pins[pin].input.type){
                            case "analog_single":
                                if(!list.includes(this._pins[pin].input.pin)){
                                    list.push(this._pins[pin].input.pin);
                                   resolve();
                                }
                            break;
                            case "analog_dual":
                                if(!list.includes(this._pins[pin].input.pins["1"])){
                                    list.push(this._pins[pin].input.pins["1"]);
                                }
                                if(!list.includes(this._pins[pin].input.pins["2"])){
                                    list.push(this._pins[pin].input.pins["2"]);
                                }
                                resolve();
                            break;
                            default:
                                resolve();
                            break;
                        }
                        
                    }
                    else{
                        resolve();
                    }
                }))
            });
            
        }

        return Promise.all(promises).then(function() {
            return list;
        });
    }
}