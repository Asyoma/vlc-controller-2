import { CONSOLE_LOG } from "./console.log";
import { AnalogSingle } from './ArduinoReaders/analogSingle';
import { AnalogDual } from './ArduinoReaders/analogDual';
import { OnStart } from "./ArduinoReaders/onStart";


export class Readers{
    readers_list={};
    constructor(pins){
        const pin_keys = Object.keys(pins);
        pin_keys.forEach(pin_key => {
            if(pins[pin_key].status == true){
                switch(pins[pin_key].input.type){
                    case "analog_single":
                        const p = pins[pin_key].input.pin.toUpperCase();
                        if(p.startsWith("A")){
    
                            this.readers_list[pin_key] = new AnalogSingle(pins[pin_key]);
                        }
                    break;
                    case "analog_dual":
                        const p1 = pins[pin_key].input.pins["1"].toUpperCase();
                        const p2 = pins[pin_key].input.pins["2"].toUpperCase();
                        if(p1.startsWith("A") && p2.startsWith("A")){
                            this.readers_list[pin_key] = new AnalogDual(pins[pin_key]);
                        }
                    break;
                    case "on_start":
                        this.readers_list[pin_key] = new OnStart(pins[pin_key]);
                    break;
                    case "digital_single":
                        
                    break;
                    case "digital_dual":
                       
                    break;
                }
            }
            
        });
    }
}