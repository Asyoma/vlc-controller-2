import { EVENT_EMITTER } from "../eventEmitter";
import { ActionMaker } from "../actionMaker";
import { CONSOLE_LOG } from "../console.log";
import { DateTime } from "luxon";

export class AnalogSingle{
    resetTimer = null;
    actionTime = 0;
    isLockdown = false;
    isAction = false;
    pin;
    settings;
    constructor(settings){
        this.settings = settings;
        this.pin = this.settings.input.pin;
            EVENT_EMITTER.on(this.pin,value=>{
                
                if(this.isLockdown == false){
                    if(this.isAction == false){
                        if(settings.value.action.min<= value && value<=settings.value.action.max){
                            this.isAction = true;
                            const now = DateTime.now();
                            this.actionTime = now.toMillis();
                            if(settings.value.readOnly == false){
                                ActionMaker(settings.command);
                            }
                            if(settings.value.showAll == false){
                                CONSOLE_LOG.info(this.pin + " status = true (" +value+")");
                            }
                            
                            
                        }
                    }
                    else{
                        if(settings.value.non_action.min<= value && value<=settings.value.non_action.max){
                            this.isAction = false;
                            if(settings.value.showAll == false){
                                CONSOLE_LOG.info(this.pin + " status = false (" +value+")");
                            }
                            if(settings.value.lockdown.status == true){
                                this.isLockdown = true;
                                const time = Number(settings.value.lockdown.time)*1000;
                                setTimeout(()=>{
                                    this.isLockdown = false;
                                    this.resetActionValue();
                                },time);
                            }
                            else{
                                this.resetActionValue();
                            }
                        }
                    }
                    if(settings.value.showAll == true){
                        CONSOLE_LOG.info(this.pin + " status = "+this.isAction+" (" +value+")");
                    }
                }
                
            });
        
    }

    private resetActionValue(){
        clearTimeout(this.resetTimer);
        if(this.isLockdown == false){
            const now = DateTime.now();
            const currentTime =  now.toMillis();
            const diff = currentTime - this.actionTime;
            if(diff >= 30000){
                if(this.isAction == true){
                    CONSOLE_LOG.info(this.pin + " force status set = false");
                    this.isAction = false;
                }
            }else{
                this.resetTimer = setTimeout(()=>{
                    this.resetActionValue()
                },5000);
            }
        }else{
            this.resetTimer = setTimeout(()=>{
                this.resetActionValue()
            },5000);
        }
        
    }
}