import { EVENT_EMITTER } from "../eventEmitter";
import { CONSOLE_LOG } from "../console.log";
import { ActionMaker } from "../actionMaker";
import { DateTime } from "luxon";
export class AnalogDual{
    pin1="";
    pin2="";
    pin_1_time=0;
    pin_2_time=0;
    pin_1_status = false;
    pin_2_status = false;
    isLockdown = false;
    resetTimer=null;

    constructor(settings){
        this.pin1 = settings.input.pins["1"].toUpperCase();
        this.pin2 = settings.input.pins["2"].toUpperCase();
        EVENT_EMITTER.on(this.pin1,value=>{
            if(this.isLockdown == false){
                if(this.pin_1_status == false && this.pin_2_status == false && settings.value.action.min<= value && value<=settings.value.action.max){
                    
                    if(settings.value.showAll == false){
                        CONSOLE_LOG.info(this.pin1 + " status = true (" +value+")");
                    }
                    const now = DateTime.now();
                    this.pin_1_time = now.toMillis();
                    this.pin_1_status = true;
                }
                else if(this.pin_1_status == true && settings.value.non_action.min<= value && value<=settings.value.non_action.max){
                    this.pin_1_status = false;
                    if(settings.value.showAll == false){
                        CONSOLE_LOG.info(this.pin1 + " status = false (" +value+")");
                    }
                }
                
            }
            if(settings.value.showAll == true){
                CONSOLE_LOG.info(this.pin1 + " status = "+this.pin_1_status+" (" +value+")");
            }
        });
        EVENT_EMITTER.on(this.pin2,value=>{
            if(this.isLockdown == false){
                if(this.pin_1_status == true && this.pin_2_status == false && settings.value.action.min<= value && value<=settings.value.action.max){
                    
                    if(settings.value.showAll == false){
                        CONSOLE_LOG.info(this.pin2 + " status = true (" +value+")");
                    }
                    const now = DateTime.now();
                    this.pin_2_time = now.toMillis();
                    this.pin_2_status = true;
                    if(this.pin_1_time<this.pin_2_time){
                        if(settings.value.readOnly == false){
                            ActionMaker(settings.command);
                        }
                        if(settings.value.lockdown.status == true){
                            this.isLockdown = true;
                            const time = Number(settings.value.lockdown.time)*1000;
                            setTimeout(()=>{
                                this.isLockdown = false;
                                this.resetActionValue();
                            },time);
                        }
                        else{
                            this.resetActionValue();
                        }
                    }
                    
                }
                else if(this.pin_2_status == true && settings.value.non_action.min<= value && value<=settings.value.non_action.max){
                    this.pin_2_status = false;
                    if(settings.value.showAll == false){
                        CONSOLE_LOG.info(this.pin2 + " status = false (" +value+")");
                    }
                    
                }
            }
            if(settings.value.showAll == true){
                CONSOLE_LOG.info(this.pin2 + " status = "+this.pin_2_status+" (" +value+")");
            }
            
        });

        
    }

    private resetActionValue(){
        clearTimeout(this.resetTimer);
        if(this.isLockdown == false){
            const now = DateTime.now();
            const currentTime =  now.toMillis();
            const diff1 = currentTime - this.pin_1_time;
            const diff2 = currentTime - this.pin_2_time;

            if(diff1 >= 30000 && diff2 >= 30000){
                if(this.pin_1_status == true){
                    this.pin_1_status = false;
                    CONSOLE_LOG.info(this.pin1 + " force status set = false");
                }
                if(this.pin_2_status == true){
                    this.pin_2_status = false;
                    CONSOLE_LOG.info(this.pin2 + " force status set = false");
                }
            }else{
                this.resetTimer = setTimeout(()=>{
                    this.resetActionValue()
                },5000);
            }
        }else{
            this.resetTimer = setTimeout(()=>{
                this.resetActionValue()
            },5000);
        }
    }
}