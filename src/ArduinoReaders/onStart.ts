import { EVENT_EMITTER } from "../eventEmitter";
import { ActionMaker } from "../actionMaker";

export class OnStart{
    settings
    constructor(settings){
        this.settings = settings;
        if(this.settings.options.startAfter>0){
            const x = Number(this.settings.options.startAfter)*1000;
            setTimeout(()=>{
                this.action();
            },x);
        }
        else{
            this.action();
        }
        EVENT_EMITTER.on("VLC",data=>{
            switch(data){
                case "ended":
                    if(this.settings.options.loop == true){
                        if(this.settings.options.lockdown.status == true){
                            const x = Number(this.settings.options.lockdown.time)*1000;
                            setTimeout(()=>{
                                this.action();
                            },x);
                        }
                        else{
                            this.action();
                        }
                    }
                break;
            }
        })
    }

    private action(){
        ActionMaker(this.settings.command);
    }
}

