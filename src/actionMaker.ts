import { CONSOLE_LOG } from "./console.log";
import { EVENT_EMITTER } from "./eventEmitter";
const ActionMaker = (obj) =>{
    CONSOLE_LOG.info(JSON.stringify(obj));
    EVENT_EMITTER.emit(obj.type,obj.value)
}

export{
    ActionMaker
}